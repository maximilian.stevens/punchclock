package ch.zli.m223.punchclock.service;

import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.repository.EntryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntryService {

    private final EntryRepository entryRepository;

    public EntryService(EntryRepository entryRepository) {
        this.entryRepository = entryRepository;
    }

    public List<Entry> findAll() {
        return entryRepository.findAll();
    }

    public Entry getEntryById(Long id) {
        if (entryRepository.findById(id).isPresent()) {
            return entryRepository.findById(id).get();
        }
        return null;
    }

    public Entry saveEntry(Entry entry) {
        return entryRepository.saveAndFlush(entry);
    }

    public void deleteEntry(Long id) {
        if (entryRepository.existsById(id)) {
            entryRepository.deleteById(id);
        }
    }
}
