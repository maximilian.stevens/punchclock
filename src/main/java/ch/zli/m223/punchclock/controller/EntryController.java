package ch.zli.m223.punchclock.controller;

import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.service.EntryService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/entries")
public class EntryController {

    // CategoryService um die Entries zu verwalten
    private final EntryService entryService;

    // Constructor
    public EntryController(EntryService entryService) {
        this.entryService = entryService;
    }

    // GET Request, welcher alle Entries zurückgibt
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Entry> getAllEntries() {
        return entryService.findAll();
    }

    // GET Request, welcher eine Entry anhand der ID zurückgibt
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Entry getEntryById(@PathVariable Long id) {
        return entryService.getEntryById(id);
    }

    // POST Request, welcher eine Category erstellt
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Entry createEntry(@Valid @RequestBody Entry entry) {
        return entryService.saveEntry(entry);
    }

    // POST Request, welcher eine Entry anhand der ID updatet
    @PostMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Entry updateEntry(@Valid @RequestBody Entry entry, @PathVariable Long id) {
        entry.setId(id);
        return entryService.saveEntry(entry);
    }

    // DELETE Request, welcher eine Entry anhand der ID löscht
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteEntry(@PathVariable Long id) {
        entryService.deleteEntry(id);
    }
}
