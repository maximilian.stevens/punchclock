package ch.zli.m223.punchclock.controller;

import ch.zli.m223.punchclock.domain.Role;
import ch.zli.m223.punchclock.service.RoleService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/roles")
public class RoleController {

    // RoleService um die Roles zu verwalten
    private final RoleService roleService;

    // Constructor
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    // GET Request, welcher alle Roles zurückgibt
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Role> getAllRoles() {
        return roleService.findAll();
    }

    // GET Request, welcher eine Role anhand der ID zurückgibt
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Role getRoleById(@PathVariable Long id) {
        return roleService.getRoleById(id);
    }

    // POST Request, welcher eine Role erstellt
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Role createRole(@Valid @RequestBody Role role) {
        return roleService.saveRole(role);
    }

    // POST Request, welcher eine Role anhand der ID updatet
    @PostMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Role updateRole(@Valid @RequestBody Role role) {
        return roleService.saveRole(role);
    }

    // DELETE Request, welcher eine Role anhand der ID löscht
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteRole(@PathVariable Long id) {
        roleService.deleteRole(id);
    }
}
