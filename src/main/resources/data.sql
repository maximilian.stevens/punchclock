insert into role (name) values
('Admin'),
('User');

insert into category (name) values
('Work'),
('Fake Work'),
('Epic Work');

insert into user (username, password, role_id) values
('Maximilian', '1234', 1),
('Chris', '1234', 2);

insert into entry (check_in, check_out, category_id, user_id) values
('2020-11-30T11:39:01', '2020-11-30T11:39:01', 1, 1),
('2020-11-30T11:39:01', '2020-11-30T11:39:01', 2, 2);