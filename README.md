# M223: Punchclock
Die Punchclock Applikation bietet diverse Funktionen. Man kann sich als User registrieren, einloggen und ausloggen.
Dazu kann man Einträge verwalten. Das heisst man kann sie erstellen, bearbeiten und löschen.
Wenn man ein Admin ist kann man dasselbe tun wie ein User, aber man hat Zugang zu einer Admin UI.
Hier kann der Admin alle User besichtigen. Diese kann er auch verwalten (erstellen, löschen, bearbeiten).
Ein User kann nur seine eigenen Einträge anschauen.
Mein Frontend habe ich mit React gemacht.

## Loslegen
Folgende Schritte befolgen um loszulegen:
1. Sicherstellen, dass JDK 12 installiert und in der Umgebungsvariable `path` definiert ist.
1. Ins Verzeichnis der Applikation wechseln und über die Kommandozeile mit `./gradlew bootRun` oder `./gradlew.bat bootRun` starten
1. Ein ausführbares JAR kann mit `./gradlew bootJar` oder `./gradlew.bat bootJar` erstellt werden.
2. Frontend Projekt öffnen und zuerst `npm install` eingeben, da alle libraries noch nicht installiert sind um Speicherplatz zu minimieren.
2. In Verzeichnis /frontend wechseln und dann `npm start` / `yarn start`.

Folgende Dienste stehen während der Ausführung im Profil `dev` zur Verfügung:
- REST-Schnittstelle der Applikation: http://localhost:8081
- Frontend der Applikation: http://localhost:3000
- Dashboard der H2 Datenbank: http://localhost:8081/h2-console