insert into role (name) values
('admin'),
('user');

insert into category (name) values
('category_one'),
('category_two');

insert into user (username, password, role_id) values
('Maximilian', '1234', 1),
('Chris', '1234', 2);